<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201101175515 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bank (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, location VARCHAR(100) NOT NULL, bic VARCHAR(9) NOT NULL, correspondent_account VARCHAR(20) NOT NULL, address LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bank_category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_370C0346727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bank_phone (id INT AUTO_INCREMENT NOT NULL, bank_id INT NOT NULL, phone VARCHAR(255) NOT NULL, INDEX IDX_4EE559A811C8FB41 (bank_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counterpart (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, category_id INT NOT NULL, name VARCHAR(255) NOT NULL, is_published TINYINT(1) NOT NULL, full_name VARCHAR(255) NOT NULL, legal_address VARCHAR(255) DEFAULT NULL, postal_address VARCHAR(255) DEFAULT NULL, code_okpo VARCHAR(13) DEFAULT NULL, is_prepayment TINYINT(1) NOT NULL, is_blacklisted TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_564F5C9FC54C8C93 (type_id), INDEX IDX_564F5C9F12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counterpart_category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_57BDD9BD727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counterpart_contact_person (id INT AUTO_INCREMENT NOT NULL, counterpart_id INT NOT NULL, name VARCHAR(255) NOT NULL, position VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_621FF6D3606374F2 (counterpart_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counterpart_phone (id INT AUTO_INCREMENT NOT NULL, type_id INT NOT NULL, counterpart_id INT NOT NULL, phone VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_74E2E774C54C8C93 (type_id), INDEX IDX_74E2E774606374F2 (counterpart_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counterpart_settlement_account (id INT AUTO_INCREMENT NOT NULL, account_bank_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, number VARCHAR(20) NOT NULL, correspondent LONGTEXT DEFAULT NULL, appointment LONGTEXT DEFAULT NULL, show_reason_code TINYINT(1) NOT NULL, INDEX IDX_8AAF0342D8E3EF09 (account_bank_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE counterpart_type (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phone_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bank_category ADD CONSTRAINT FK_370C0346727ACA70 FOREIGN KEY (parent_id) REFERENCES bank_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bank_phone ADD CONSTRAINT FK_4EE559A811C8FB41 FOREIGN KEY (bank_id) REFERENCES bank (id)');
        $this->addSql('ALTER TABLE counterpart ADD CONSTRAINT FK_564F5C9FC54C8C93 FOREIGN KEY (type_id) REFERENCES counterpart_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE counterpart ADD CONSTRAINT FK_564F5C9F12469DE2 FOREIGN KEY (category_id) REFERENCES counterpart_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE counterpart_category ADD CONSTRAINT FK_57BDD9BD727ACA70 FOREIGN KEY (parent_id) REFERENCES counterpart_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE counterpart_contact_person ADD CONSTRAINT FK_621FF6D3606374F2 FOREIGN KEY (counterpart_id) REFERENCES counterpart (id)');
        $this->addSql('ALTER TABLE counterpart_phone ADD CONSTRAINT FK_74E2E774C54C8C93 FOREIGN KEY (type_id) REFERENCES phone_type (id)');
        $this->addSql('ALTER TABLE counterpart_phone ADD CONSTRAINT FK_74E2E774606374F2 FOREIGN KEY (counterpart_id) REFERENCES counterpart (id)');
        $this->addSql('ALTER TABLE counterpart_settlement_account ADD CONSTRAINT FK_8AAF0342D8E3EF09 FOREIGN KEY (account_bank_id) REFERENCES bank (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank_phone DROP FOREIGN KEY FK_4EE559A811C8FB41');
        $this->addSql('ALTER TABLE counterpart_settlement_account DROP FOREIGN KEY FK_8AAF0342D8E3EF09');
        $this->addSql('ALTER TABLE bank_category DROP FOREIGN KEY FK_370C0346727ACA70');
        $this->addSql('ALTER TABLE counterpart_contact_person DROP FOREIGN KEY FK_621FF6D3606374F2');
        $this->addSql('ALTER TABLE counterpart_phone DROP FOREIGN KEY FK_74E2E774606374F2');
        $this->addSql('ALTER TABLE counterpart DROP FOREIGN KEY FK_564F5C9F12469DE2');
        $this->addSql('ALTER TABLE counterpart_category DROP FOREIGN KEY FK_57BDD9BD727ACA70');
        $this->addSql('ALTER TABLE counterpart DROP FOREIGN KEY FK_564F5C9FC54C8C93');
        $this->addSql('ALTER TABLE counterpart_phone DROP FOREIGN KEY FK_74E2E774C54C8C93');
        $this->addSql('DROP TABLE bank');
        $this->addSql('DROP TABLE bank_category');
        $this->addSql('DROP TABLE bank_phone');
        $this->addSql('DROP TABLE counterpart');
        $this->addSql('DROP TABLE counterpart_category');
        $this->addSql('DROP TABLE counterpart_contact_person');
        $this->addSql('DROP TABLE counterpart_phone');
        $this->addSql('DROP TABLE counterpart_settlement_account');
        $this->addSql('DROP TABLE counterpart_type');
        $this->addSql('DROP TABLE phone_type');
        $this->addSql('DROP TABLE `user`');
    }
}
