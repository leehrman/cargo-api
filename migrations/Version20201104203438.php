<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201104203438 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE counterpart_contract (id INT AUTO_INCREMENT NOT NULL, fine_time_period_type_id INT DEFAULT NULL, counterpart_id INT NOT NULL, name VARCHAR(100) NOT NULL, number VARCHAR(50) NOT NULL, contract_date DATETIME NOT NULL, obligation_date DATETIME DEFAULT NULL, obligation_liquidation_date DATETIME DEFAULT NULL, fine_start_date DATETIME NOT NULL, fine_rate INT NOT NULL, INDEX IDX_B87EE825F9D638 (fine_time_period_type_id), INDEX IDX_B87EE825606374F2 (counterpart_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE time_period_unit (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE counterpart_contract ADD CONSTRAINT FK_B87EE825F9D638 FOREIGN KEY (fine_time_period_type_id) REFERENCES time_period_unit (id)');
        $this->addSql('ALTER TABLE counterpart_contract ADD CONSTRAINT FK_B87EE825606374F2 FOREIGN KEY (counterpart_id) REFERENCES counterpart (id)');
        $this->addSql('ALTER TABLE bank CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL, CHANGE name name VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE account_bank_id account_bank_id INT NOT NULL, CHANGE correspondent_bank_id correspondent_bank_id INT DEFAULT NULL, CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE counterpart_contract DROP FOREIGN KEY FK_B87EE825F9D638');
        $this->addSql('DROP TABLE counterpart_contract');
        $this->addSql('DROP TABLE time_period_unit');
        $this->addSql('ALTER TABLE bank CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL, CHANGE name name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE account_bank_id account_bank_id INT NOT NULL, CHANGE correspondent_bank_id correspondent_bank_id INT DEFAULT NULL, CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
