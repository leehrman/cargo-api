<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201104211741 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_contract ADD fine_end_date DATETIME DEFAULT NULL, CHANGE counterpart_id counterpart_id INT NOT NULL, CHANGE contract_date contract_date DATETIME NOT NULL, CHANGE obligation_date obligation_date DATETIME DEFAULT NULL, CHANGE obligation_liquidation_date obligation_liquidation_date DATETIME DEFAULT NULL, CHANGE fine_start_date fine_start_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE account_bank_id account_bank_id INT NOT NULL, CHANGE correspondent_bank_id correspondent_bank_id INT DEFAULT NULL, CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_contract DROP fine_end_date, CHANGE counterpart_id counterpart_id INT NOT NULL, CHANGE contract_date contract_date DATETIME NOT NULL, CHANGE obligation_date obligation_date DATETIME DEFAULT NULL, CHANGE obligation_liquidation_date obligation_liquidation_date DATETIME DEFAULT NULL, CHANGE fine_start_date fine_start_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE account_bank_id account_bank_id INT NOT NULL, CHANGE correspondent_bank_id correspondent_bank_id INT DEFAULT NULL, CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
