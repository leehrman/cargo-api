<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201101183940 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account DROP FOREIGN KEY FK_8AAF0342D8E3EF09');
        $this->addSql('DROP INDEX IDX_8AAF0342D8E3EF09 ON counterpart_settlement_account');
        $this->addSql('ALTER TABLE counterpart_settlement_account DROP account_bank_id, CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account ADD account_bank_id INT DEFAULT NULL, CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account ADD CONSTRAINT FK_8AAF0342D8E3EF09 FOREIGN KEY (account_bank_id) REFERENCES bank (id)');
        $this->addSql('CREATE INDEX IDX_8AAF0342D8E3EF09 ON counterpart_settlement_account (account_bank_id)');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
