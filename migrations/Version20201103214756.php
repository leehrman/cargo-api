<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103214756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank DROP FOREIGN KEY FK_D860BF7A11C8FB41');
        $this->addSql('DROP INDEX IDX_D860BF7A11C8FB41 ON bank');
        $this->addSql('ALTER TABLE bank ADD category_id INT NOT NULL, DROP bank_id, CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE bank ADD CONSTRAINT FK_D860BF7A12469DE2 FOREIGN KEY (category_id) REFERENCES bank_category (id)');
        $this->addSql('CREATE INDEX IDX_D860BF7A12469DE2 ON bank (category_id)');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE account_bank_id account_bank_id INT NOT NULL, CHANGE correspondent_bank_id correspondent_bank_id INT DEFAULT NULL, CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bank DROP FOREIGN KEY FK_D860BF7A12469DE2');
        $this->addSql('DROP INDEX IDX_D860BF7A12469DE2 ON bank');
        $this->addSql('ALTER TABLE bank ADD bank_id INT NOT NULL, DROP category_id, CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE bank ADD CONSTRAINT FK_D860BF7A11C8FB41 FOREIGN KEY (bank_id) REFERENCES bank_category (id)');
        $this->addSql('CREATE INDEX IDX_D860BF7A11C8FB41 ON bank (bank_id)');
        $this->addSql('ALTER TABLE bank_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_phone CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE bank_id bank_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE type_id type_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_category CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE parent_id parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE counterpart_contact_person CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_phone CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_settlement_account CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE account_bank_id account_bank_id INT NOT NULL, CHANGE correspondent_bank_id correspondent_bank_id INT DEFAULT NULL, CHANGE counterpart_id counterpart_id INT NOT NULL');
        $this->addSql('ALTER TABLE counterpart_type CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
