<?php

namespace App\DataFixtures;

use App\Entity\BankCategory;
use App\Entity\CounterpartCategory;
use App\Entity\CounterpartContactPerson;
use App\Entity\CounterpartSettlementAccount;
use App\Entity\CounterpartType;
use App\Entity\PhoneType;
use App\Entity\TimePeriodUnit;
use App\Entity\User;
use App\Factory\BankCategoryFactory;
use App\Factory\BankFactory;
use App\Factory\CounterpartContactPersonFactory;
use App\Factory\CounterpartFactory;
use App\Factory\CounterpartPhoneFactory;
use App\Factory\CounterpartSettlementAccountFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Factory\CounterpartCategoryFactory;
use Zenstruck\Foundry\Proxy;

class AppFixtures extends Fixture
{
    private UserPasswordEncoderInterface $passwordEncoder;

    private ObjectManager $objectManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->objectManager = $manager;

        $this->createUsers();
        $this->createPhoneTypes();
        $counterpartTypes = $this->creteCounterpartTypes();
        $categories = $this->createCounterpartCategories();
        $this->createBankCategories();
        $this->createBanks();
        $this->creteTimePeriodUnits();
        $this->createCounterparts($categories, $counterpartTypes);
    }

    /**
     * @param array $categories
     * @param CounterpartType[] $types
     * @return $this
     */
    private function createCounterParts(array $categories, array $types): self
    {
        for ($i = 0; $i <= 20; $i++) {
            CounterpartFactory::new()->setRandomCategory($categories)->setRandomType($types)->create();
        }
        $counterparts = CounterpartFactory::randomSet(20);
        foreach ($counterparts as $counterpart) {
            CounterpartPhoneFactory::new()->createMany(2, ['counterpart' => $counterpart]);
            CounterpartContactPersonFactory::new()->createMany(2, ['counterpart' => $counterpart]);
        }

        return $this;
    }

    /**
     * @return AppFixtures
     */
    private function createUsers(): AppFixtures
    {
        $user = new User();
        $user
            ->setEmail('admin@admin.admin')
            ->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    'admin'
                )
            );
        $this->objectManager->persist($user);
        $this->objectManager->flush();

        return $this;
    }

    private function createCounterpartCategories(): array
    {
        $factory = CounterpartCategoryFactory::new();
        $factory->createMany(2);
        $rootCategory = $factory::random();
        $result[] = $rootCategory;

        CounterpartCategoryFactory::new()->createMany(3, ['parent' => $rootCategory]);
        $subCategories = CounterpartCategoryFactory::randomSet(3);
        $result = array_merge($subCategories);

        CounterpartCategoryFactory::new()->setRandomParent($subCategories)->createMany(3);

        return array_merge($result, CounterpartCategoryFactory::randomSet(3));
    }

    /**
     * @return $this
     */
    private function createBanks(): self
    {
        BankFactory::new()->createMany(30);

        return $this;
    }

    /**
     * @return $this
     */
    private function createBankCategories(): self
    {
        $factory = BankCategoryFactory::new();
        $factory->createMany(2);
        $rootCategory = $factory::random();
        $result[] = $rootCategory;

        BankCategoryFactory::new()->createMany(3, ['parent' => $rootCategory]);
        $subCategories = BankCategoryFactory::randomSet(3);

        BankCategoryFactory::new()->setRandomParent($subCategories)->createMany(3);

        return $this;
    }

    private function creteCounterpartTypes(): array
    {
        $types = [
            [
                'id' => CounterpartType::ID_NATURAL_PERSON,
                'code' => CounterpartType::CODE_NATURAL_PERSON,
                'name' => 'Физ.лицо',
            ],
            [
                'id' => CounterpartType::ID_SELF_EMPLOYED,
                'code' => CounterpartType::CODE_SELF_EMPLOYED,
                'name' => 'ИП',
            ],
            [
                'id' => CounterpartType::ID_OOO,
                'code' => CounterpartType::CODE_OOO,
                'name' => 'ООО',
            ],
            [
                'id' => CounterpartType::ID_OTHERS,
                'code' => CounterpartType::CODE_OTHERS,
                'name' => 'Прочее',
            ],
        ];
        $result = [];
        foreach ($types as $typeData) {
            $type = (new CounterpartType())
                ->setId($typeData['id'])
                ->setCode($typeData['code'])
                ->setName($typeData['name']);
            $this->objectManager->persist($type);
            $result[] = $type;
        }
        $this->objectManager->flush();

        return $result;
    }

    /**
     * @return array
     */
    private function createPhoneTypes(): array
    {
        $result = [];
        $type = (new PhoneType())->setId(PhoneType::ID_WORK)->setName('Раб.');
        $this->objectManager->persist($type);
        $result[] = $type;

        $type = (new PhoneType())->setId(PhoneType::ID_MOBILE)->setName('Моб.');
        $this->objectManager->persist($type);
        $result[] = $type;

        $this->objectManager->flush();

        return $result;
    }

    /**
     * @return array
     */
    private function creteTimePeriodUnits(): array
    {
        $units = [
            [
                'id' => TimePeriodUnit::TYPE_DAY,
                'name' => 'День',
            ],
            [
                'id' => TimePeriodUnit::TYPE_WORK_DAY,
                'name' => 'Рабочий день',
            ],
            [
                'id' => TimePeriodUnit::TYPE_WEEK,
                'name' => 'Неделя',
            ],
            [
                'id' => TimePeriodUnit::TYPE_MONTH,
                'name' => 'Месяц',
            ],
        ];
        foreach ($units as $unit) {
            $type = (new TimePeriodUnit())
                ->setId($unit['id'])
                ->setName($unit['name']);
            $this->objectManager->persist($type);
            $result[] = $type;
        }
        $this->objectManager->flush();

        return $result;
    }
}
