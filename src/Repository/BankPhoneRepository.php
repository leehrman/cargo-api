<?php

namespace App\Repository;

use App\Entity\BankPhone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BankPhone|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankPhone|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankPhone[]    findAll()
 * @method BankPhone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankPhoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankPhone::class);
    }

    // /**
    //  * @return BankPhone[] Returns an array of BankPhone objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankPhone
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
