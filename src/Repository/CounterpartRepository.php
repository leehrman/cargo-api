<?php

namespace App\Repository;

use App\Entity\Counterpart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Counterpart|null find($id, $lockMode = null, $lockVersion = null)
 * @method Counterpart|null findOneBy(array $criteria, array $orderBy = null)
 * @method Counterpart[]    findAll()
 * @method Counterpart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Counterpart::class);
    }

    // /**
    //  * @return Counterpart[] Returns an array of Counterpart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Counterpart
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
