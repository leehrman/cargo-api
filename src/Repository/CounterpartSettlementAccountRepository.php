<?php

namespace App\Repository;

use App\Entity\CounterpartSettlementAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CounterpartSettlementAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterpartSettlementAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterpartSettlementAccount[]    findAll()
 * @method CounterpartSettlementAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartSettlementAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterpartSettlementAccount::class);
    }

    // /**
    //  * @return CounterpartSettlementAccount[] Returns an array of CounterpartSettlementAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CounterpartSettlementAccount
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
