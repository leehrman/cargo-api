<?php

namespace App\Repository;

use App\Entity\CounterpartType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CounterpartType|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterpartType|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterpartType[]    findAll()
 * @method CounterpartType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterpartType::class);
    }

    // /**
    //  * @return CounterpartType[] Returns an array of CounterpartType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CounterpartType
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
