<?php

namespace App\Repository;

use App\Entity\CounterpartCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CounterpartCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterpartCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterpartCategory[]    findAll()
 * @method CounterpartCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterpartCategory::class);
    }

    // /**
    //  * @return CounterpartCategory[] Returns an array of CounterpartCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CounterpartCategory
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
