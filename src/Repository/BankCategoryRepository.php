<?php

namespace App\Repository;

use App\Entity\BankCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BankCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankCategory[]    findAll()
 * @method BankCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankCategory::class);
    }

    // /**
    //  * @return BankCategory[] Returns an array of BankCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankCategory
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
