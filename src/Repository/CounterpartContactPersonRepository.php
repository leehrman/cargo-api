<?php

namespace App\Repository;

use App\Entity\CounterpartContactPerson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CounterpartContactPerson|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterpartContactPerson|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterpartContactPerson[]    findAll()
 * @method CounterpartContactPerson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartContactPersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterpartContactPerson::class);
    }

    // /**
    //  * @return CounterpartContactPerson[] Returns an array of CounterpartContactPerson objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CounterpartContactPerson
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
