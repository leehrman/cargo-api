<?php

namespace App\Repository;

use App\Entity\CounterpartPhone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CounterpartPhone|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterpartPhone|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterpartPhone[]    findAll()
 * @method CounterpartPhone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartPhoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterpartPhone::class);
    }

    // /**
    //  * @return CounterpartPhone[] Returns an array of CounterpartPhone objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CounterpartPhone
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
