<?php

namespace App\Repository;

use App\Entity\CounterpartContract;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CounterpartContract|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterpartContract|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterpartContract[]    findAll()
 * @method CounterpartContract[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartContractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterpartContract::class);
    }

    // /**
    //  * @return CounterpartContract[] Returns an array of CounterpartContract objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CounterpartContract
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
