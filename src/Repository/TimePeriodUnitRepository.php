<?php

namespace App\Repository;

use App\Entity\TimePeriodUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TimePeriodUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimePeriodUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimePeriodUnit[]    findAll()
 * @method TimePeriodUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimePeriodUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimePeriodUnit::class);
    }

    // /**
    //  * @return TimePeriodUnit[] Returns an array of TimePeriodUnit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TimePeriodUnit
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
