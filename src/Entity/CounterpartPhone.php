<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CounterpartPhoneRepository;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CounterpartPhoneRepository::class)
 */
class CounterpartPhone implements TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"counterpart:read"})
     */
    private ?string $phone;

    /**
     * @ORM\ManyToOne(targetEntity=PhoneType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ?PhoneType $type;

    /**
     * @ORM\ManyToOne(targetEntity=Counterpart::class, inversedBy="phones")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Counterpart $counterpart;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getType(): ?PhoneType
    {
        return $this->type;
    }

    /**
     * @Groups({"counterpart:read"})
     * @SerializedName("type")
     * @return string|null
     */
    public function getPhoneType(): ?string
    {
        return $this->type->getName();
    }

    public function setType(?PhoneType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCounterpart(): ?Counterpart
    {
        return $this->counterpart;
    }

    public function setCounterpart(?Counterpart $counterpart): self
    {
        $this->counterpart = $counterpart;

        return $this;
    }
}
