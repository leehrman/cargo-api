<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CounterpartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\{Groups, SerializedName};
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"counterpart:read"}},
 *     denormalizationContext={"groups"={"counterpart:write"}},
 * )
 * @ORM\Entity(repositoryClass=CounterpartRepository::class)
 */
class Counterpart implements TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"UNSIGNED":true})
     * @Groups({"counterpart:read"})
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"counterpart:read", "counterpart:write"})
     * @Assert\NotBlank()
     * @Assert\Length(max=50)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"counterpart:read"})
     */
    private ?bool $isPublished = true;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"counterpart:read", "counterpart:write"})
     * @Assert\Length(max=255)
     */
    private ?string $fullName;

    /**
     * @ORM\ManyToOne(targetEntity=CounterpartType::class)
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private ?CounterpartType $type;

    /**
     * @ORM\ManyToOne(targetEntity=CounterpartCategory::class, inversedBy="counterparts")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Groups({"counterpart:read", "counterpart:write"})
     */
    private ?CounterpartCategory $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"counterpart:read", "counterpart:write"})
     * @Assert\Length(max=255)
     */
    private ?string $legalAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"counterpart:read", "counterpart:write"})
     */
    private ?string $postalAddress;

    /**
     * @ORM\OneToMany(targetEntity=CounterpartPhone::class, mappedBy="counterpart", orphanRemoval=true)
     * @Groups({"counterpart:read"})
     */
    private Collection $phones;

    /**
     * @ORM\OneToMany(targetEntity=CounterpartContactPerson::class, mappedBy="counterpart", orphanRemoval=true)
     * @Groups({"counterpart:read"})
     */
    private Collection $contacts;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     * @Groups({"counterpart:read", "counterpart:write"})
     */
    private ?string $codeOKPO;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"counterpart:read", "counterpart:write"})
     */
    private bool $isPrepayment;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"counterpart:read", "counterpart:write"})
     */
    private ?bool $isBlacklisted;

    /**
     * @ORM\OneToMany(targetEntity=CounterpartSettlementAccount::class, mappedBy="counterpart", orphanRemoval=true)
     * @Groups({"counterpart:read", "counterpart:write"})
     */
    private $settlementAccounts;

    /**
     * @ORM\OneToMany(targetEntity=CounterpartContract::class, mappedBy="counterpart", orphanRemoval=true)
     * @Groups({"counterpart:read"})
     */
    private $contracts;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->settlementAccounts = new ArrayCollection();
        $this->contracts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getType(): ?CounterpartType
    {
        return $this->type;
    }

    /**
     * @Groups({"counterpart:read"})
     * @SerializedName("type")
     * @return string|null
     */
    public function getTypeName(): ?string
    {
        return $this->type->getName();
    }


    public function setType(?CounterpartType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCategory(): ?CounterpartCategory
    {
        return $this->category;
    }

    public function setCategory(?CounterpartCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @Groups({"counterpart:read"})
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return null !== $this->category ? $this->category->getId() : null;
    }

    public function getLegalAddress(): ?string
    {
        return $this->legalAddress;
    }

    public function setLegalAddress(?string $legalAddress): self
    {
        $this->legalAddress = $legalAddress;

        return $this;
    }

    public function getPostalAddress(): ?string
    {
        return $this->postalAddress;
    }

    public function setPostalAddress(?string $postalAddress): self
    {
        $this->postalAddress = $postalAddress;

        return $this;
    }

    /**
     * @return Collection|CounterpartPhone[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    public function addPhone(CounterpartPhone $phone): self
    {
        if (!$this->phones->contains($phone)) {
            $this->phones[] = $phone;
            $phone->setCounterpart($this);
        }

        return $this;
    }

    public function removePhone(CounterpartPhone $phone): self
    {
        if ($this->phones->contains($phone)) {
            $this->phones->removeElement($phone);
            // set the owning side to null (unless already changed)
            if ($phone->getCounterpart() === $this) {
                $phone->setCounterpart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CounterpartContactPerson[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(CounterpartContactPerson $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setCounterpart($this);
        }

        return $this;
    }

    public function removeContact(CounterpartContactPerson $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getCounterpart() === $this) {
                $contact->setCounterpart(null);
            }
        }

        return $this;
    }

    public function getCodeOKPO(): ?string
    {
        return $this->codeOKPO;
    }

    public function setCodeOKPO(?string $codeOKPO): self
    {
        $this->codeOKPO = $codeOKPO;

        return $this;
    }

    public function getIsPrepayment(): ?bool
    {
        return $this->isPrepayment;
    }

    public function setIsPrepayment(bool $isPrepayment): self
    {
        $this->isPrepayment = $isPrepayment;

        return $this;
    }

    public function getIsBlacklisted(): ?bool
    {
        return $this->isBlacklisted;
    }

    public function setIsBlacklisted(bool $isBlacklisted): self
    {
        $this->isBlacklisted = $isBlacklisted;

        return $this;
    }

    /**
     * @return Collection|CounterpartSettlementAccount[]
     */
    public function getSettlementAccounts(): Collection
    {
        return $this->settlementAccounts;
    }

    public function addSettlementAccount(CounterpartSettlementAccount $settlementAccount): self
    {
        if (!$this->settlementAccounts->contains($settlementAccount)) {
            $this->settlementAccounts[] = $settlementAccount;
            $settlementAccount->setCounterpart($this);
        }

        return $this;
    }

    public function removeSettlementAccount(CounterpartSettlementAccount $settlementAccount): self
    {
        // set the owning side to null (unless already changed)
        if ($this->settlementAccounts->removeElement($settlementAccount)
            && $settlementAccount->getCounterpart() === $this) {
            $settlementAccount->setCounterpart(null);
        }

        return $this;
    }

    /**
     * @return Collection|CounterpartContract[]
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(CounterpartContract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->setCounterpart($this);
        }

        return $this;
    }

    public function removeContract(CounterpartContract $contract): self
    {
        // set the owning side to null (unless already changed)
        if ($this->contracts->removeElement($contract)
            && $contract->getCounterpart() === $this) {
            $contract->setCounterpart(null);
        }

        return $this;
    }
}
