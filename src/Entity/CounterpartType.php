<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CounterpartTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CounterpartTypeRepository::class)
 */
class CounterpartType
{
    public const ID_NATURAL_PERSON = 1;
    public const ID_SELF_EMPLOYED = 2;
    public const ID_OOO = 3;
    public const ID_OTHERS = 10;

    public const CODE_NATURAL_PERSON = 'natural_person';
    public const CODE_SELF_EMPLOYED = 'self_employed';
    public const CODE_OOO = 'ooo';
    public const CODE_OTHERS = 'others';


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"UNSIGNED":true})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): CounterpartType
    {
        $this->id = $id;
        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
