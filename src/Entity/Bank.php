<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BankRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *  * @ApiResource(
 *     normalizationContext={"groups"={"bank:read"}},
 *     denormalizationContext={"groups"={"bank:write"}},
 * )
 * @ORM\Entity(repositoryClass=BankRepository::class)
 */
class Bank implements TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"UNSIGNED":true})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"bank:read", "bank:write"})
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"bank:read", "bank:write"})
     */
    private ?string $location;

    /**
     * @ORM\Column(type="string", length=9, unique=true)
     * @Groups({"bank:read", "bank:write"})
     * @Assert\Type("numeric")
     * @Assert\Length(9)
     */
    private ?string $bic;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"bank:read", "bank:write"})
     * @Assert\Type("numeric")
     * @Assert\Length(9)
     */
    private ?string $correspondentAccount;

    /**
     * @ORM\Column(type="text")
     * @Groups({"bank:read", "bank:write"})
     */
    private ?string $address;

    /**
     * @ORM\OneToMany(targetEntity=BankPhone::class, mappedBy="bank", orphanRemoval=true)
     * @Groups({"bank:read"})
     */
    private $phones;

    /**
     * @ORM\ManyToOne(targetEntity=BankCategory::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getBic(): ?string
    {
        return $this->bic;
    }

    public function setBic(string $bic): self
    {
        $this->bic = $bic;

        return $this;
    }

    public function getCorrespondentAccount(): ?string
    {
        return $this->correspondentAccount;
    }

    public function setCorrespondentAccount(string $correspondentAccount): self
    {
        $this->correspondentAccount = $correspondentAccount;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|BankPhone[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    public function addPhone(BankPhone $phone): self
    {
        if (!$this->phones->contains($phone)) {
            $this->phones[] = $phone;
            $phone->setBank($this);
        }

        return $this;
    }

    public function removePhone(BankPhone $phone): self
    {
        // set the owning side to null (unless already changed)
        if ($this->phones->removeElement($phone) && $phone->getBank() === $this) {
            $phone->setBank(null);
        }

        return $this;
    }

    public function getCategory(): ?BankCategory
    {
        return $this->category;
    }

    public function setCategory(?BankCategory $category): self
    {
        $this->category = $category;

        return $this;
    }
}
