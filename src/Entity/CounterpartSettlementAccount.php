<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CounterpartSettlementAccountRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"counterpart_settlement_account:read"}},
 *     denormalizationContext={"groups"={"counterpart_settlement_account:write"}},
 * )
 * @ORM\Entity(repositoryClass=CounterpartSettlementAccountRepository::class)
 */
class CounterpartSettlementAccount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"UNSIGNED":true})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"counterpart_settlement_account:read", "counterpart_settlement_account:write"})
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"counterpart_settlement_account:read", "counterpart_settlement_account:write"})
     * @Assert\Length(20)
     * @Assert\Type("numeric")
     */
    private ?string $number;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Groups({"counterpart_settlement_account:read", "counterpart_settlement_account:write"})
     */
    private ?string $correspondent;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"counterpart_settlement_account:read", "counterpart_settlement_account:write"})
     */
    private ?string $appointment;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"counterpart_settlement_account:read", "counterpart_settlement_account:write"})
     */
    private ?bool $showReasonCode = false;

    /**
     * @ORM\ManyToOne(targetEntity=Bank::class)
     * @Groups({"counterpart_settlement_account:read", "counterpart_settlement_account:write"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $accountBank;

    /**
     * @ORM\ManyToOne(targetEntity=Bank::class)
     * @Groups({"counterpart_settlement_account:read", "counterpart_settlement_account:write"})
     */
    private $correspondentBank;

    /**
     * @ORM\ManyToOne(targetEntity=Counterpart::class, inversedBy="settlementAccounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $counterpart;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getCorrespondent(): ?string
    {
        return $this->correspondent;
    }

    public function setCorrespondent(?string $correspondent): self
    {
        $this->correspondent = $correspondent;

        return $this;
    }

    public function getAppointment(): ?string
    {
        return $this->appointment;
    }

    public function setAppointment(?string $appointment): self
    {
        $this->appointment = $appointment;

        return $this;
    }

    public function getShowReasonCode(): ?bool
    {
        return $this->showReasonCode;
    }

    public function setShowReasonCode(bool $showReasonCode): self
    {
        $this->showReasonCode = $showReasonCode;

        return $this;
    }

    public function getAccountBank(): ?Bank
    {
        return $this->accountBank;
    }

    public function setAccountBank(?Bank $accountBank): self
    {
        $this->accountBank = $accountBank;

        return $this;
    }

    public function getCorrespondentBank(): ?Bank
    {
        return $this->correspondentBank;
    }

    public function setCorrespondentBank(?Bank $correspondentBank): self
    {
        $this->correspondentBank = $correspondentBank;

        return $this;
    }

    public function getCounterpart(): ?Counterpart
    {
        return $this->counterpart;
    }

    public function setCounterpart(?Counterpart $counterpart): self
    {
        $this->counterpart = $counterpart;

        return $this;
    }
}
