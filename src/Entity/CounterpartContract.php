<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CounterpartContractRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeInterface;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CounterpartContractRepository::class)
 */
class CounterpartContract
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(max=50)
     */
    private ?string $number;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private ?DateTimeInterface $contractDate;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private ?DateTimeInterface $obligationDate;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private ?DateTimeInterface $obligationLiquidationDate;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private ?DateTimeInterface $fineStartDate;

    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private ?DateTimeInterface $fineEndDate;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(max=9999999)
     */
    private ?int $fineRate;

    /**
     * @ORM\ManyToOne(targetEntity=TimePeriodUnit::class)
     */
    private ?TimePeriodUnit $fineTimePeriodType;

    /**
     * @ORM\ManyToOne(targetEntity=Counterpart::class, inversedBy="contracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Counterpart $counterpart;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getContractDate(): ?\DateTimeInterface
    {
        return $this->contractDate;
    }

    public function setContractDate(\DateTimeInterface $contractDate): self
    {
        $this->contractDate = $contractDate;

        return $this;
    }

    public function getObligationDate(): ?\DateTimeInterface
    {
        return $this->obligationDate;
    }

    public function setObligationDate(?\DateTimeInterface $obligationDate): self
    {
        $this->obligationDate = $obligationDate;

        return $this;
    }

    public function getObligationLiquidationDate(): ?\DateTimeInterface
    {
        return $this->obligationLiquidationDate;
    }

    public function setObligationLiquidationDate(?\DateTimeInterface $obligationLiquidationDate): self
    {
        $this->obligationLiquidationDate = $obligationLiquidationDate;

        return $this;
    }

    public function getFineStartDate(): ?\DateTimeInterface
    {
        return $this->fineStartDate;
    }

    public function setFineStartDate(\DateTimeInterface $fineStartDate): self
    {
        $this->fineStartDate = $fineStartDate;

        return $this;
    }

    public function getFineRate(): ?int
    {
        return $this->fineRate;
    }

    public function setFineRate(int $fineRate): self
    {
        $this->fineRate = $fineRate;

        return $this;
    }

    public function getFineTimePeriodType(): ?TimePeriodUnit
    {
        return $this->fineTimePeriodType;
    }

    public function setFineTimePeriodType(?TimePeriodUnit $fineTimePeriodType): self
    {
        $this->fineTimePeriodType = $fineTimePeriodType;

        return $this;
    }

    public function getCounterpart(): ?Counterpart
    {
        return $this->counterpart;
    }

    public function setCounterpart(?Counterpart $counterpart): self
    {
        $this->counterpart = $counterpart;

        return $this;
    }

    public function getFineEndDate(): ?\DateTimeInterface
    {
        return $this->fineEndDate;
    }

    public function setFineEndDate(?\DateTimeInterface $fineEndDate): self
    {
        $this->fineEndDate = $fineEndDate;

        return $this;
    }
}
