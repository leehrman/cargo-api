<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CounterpartContactPersonRepository;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=CounterpartContactPersonRepository::class)
 */
class CounterpartContactPerson implements TimestampableInterface
{
    use TimestampableTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"counterpart:read"})
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"counterpart:read"})
     */
    private ?string $position;

    /**
     * @ORM\ManyToOne(targetEntity=Counterpart::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $counterpart;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getCounterpart(): ?Counterpart
    {
        return $this->counterpart;
    }

    public function setCounterpart(?Counterpart $counterpart): self
    {
        $this->counterpart = $counterpart;

        return $this;
    }
}
