<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CounterpartCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"counterpart_category:read"}, "enable_max_depth"=true},
 *     denormalizationContext={"groups"={"counterpart_category:write"}},
 * )
 * @ApiFilter(ExistsFilter::class, properties={"parent"})
 * @ORM\Entity(repositoryClass=CounterpartCategoryRepository::class)
 */
class CounterpartCategory implements TimestampableInterface
{
    use TimestampableTrait;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->counterparts = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"UNSIGNED":true})
     * @Groups({"counterpart_category:read"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"counterpart_category:read", "counterpart_category:write"})
     */
    private ?string $name;

    /**
     * @ORM\ManyToOne(targetEntity=CounterpartCategory::class, inversedBy="children")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     * @Groups({"counterpart_category:read", "counterpart_category:write"})
     * @ApiProperty(readableLink=false)
     */
    private ?self $parent;

    /**
     * @ORM\OneToMany(targetEntity=CounterpartCategory::class, mappedBy="parent", orphanRemoval=true)
     * @Groups({"counterpart_category:read"})
     * @MaxDepth(3)
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity=Counterpart::class, mappedBy="category", orphanRemoval=true)
     */
    private $counterparts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * @param CounterpartCategory|null $parent
     * @return $this
     */
    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Counterpart[]
     */
    public function getCounterparts(): Collection
    {
        return $this->counterparts;
    }

    public function addCounterpart(Counterpart $counterpart): self
    {
        if (!$this->counterparts->contains($counterpart)) {
            $this->counterparts[] = $counterpart;
            $counterpart->setCategory($this);
        }

        return $this;
    }

    public function removeCounterpart(Counterpart $counterpart): self
    {
        if ($this->counterparts->contains($counterpart)) {
            $this->counterparts->removeElement($counterpart);
            // set the owning side to null (unless already changed)
            if ($counterpart->getCategory() === $this) {
                $counterpart->setCategory(null);
            }
        }

        return $this;
    }
}
