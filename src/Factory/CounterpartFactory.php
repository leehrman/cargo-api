<?php

namespace App\Factory;

use App\Entity\Counterpart;
use App\Entity\CounterpartCategory;
use App\Entity\CounterpartSettlementAccount;
use App\Entity\CounterpartType;
use App\Repository\CounterpartRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Counterpart|Proxy findOrCreate(array $attributes)
 * @method static Counterpart|Proxy random()
 * @method static Counterpart[]|Proxy[] randomSet(int $number)
 * @method static Counterpart[]|Proxy[] randomRange(int $min, int $max)
 * @method static CounterpartRepository|RepositoryProxy repository()
 * @method Counterpart|Proxy create($attributes = [])
 * @method Counterpart[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class CounterpartFactory extends ModelFactory
{
    /**
     * @param CounterpartCategory[] $categories
     * @return CounterpartFactory
     */
    public function setRandomCategory(array $categories): CounterpartFactory
    {
        return $this->addState(
            [
                'category' => self::faker()->randomElement($categories),
            ]
        );
    }

    /**
     * @param CounterpartType[] $types
     * @return $this
     */
    public function setRandomType(array $types): CounterpartFactory
    {
        return $this->addState(
            [
                'type' => self::faker()->randomElement($types),
            ]
        );
    }

    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->firstName,
            'fullName' => self::faker()->name,
            'legalAddress' => self::faker()->address,
            'postalAddress' => self::faker()->address,
            'codeOKPO' => self::faker()->numberBetween(1000000000000, 9999999999999),
            'isPrepayment' => self::faker()->boolean,
            'isBlacklisted' => self::faker()->boolean,
            'isPublished' => self::faker()->boolean,
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            ->afterPersist(
                static function (Proxy $counterpart, array $attributes) {
                    CounterpartSettlementAccountFactory::new()
                        ->addState(['counterpart' => $counterpart])
                        ->createMany(self::faker()->numberBetween(2, 5));

                    CounterpartContractFactory::new()
                        ->addState(['counterpart' => $counterpart])
                        ->createMany(self::faker()->numberBetween(1, 3));
                }
            );
    }

    protected static function getClass(): string
    {
        return Counterpart::class;
    }

}
