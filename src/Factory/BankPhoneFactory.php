<?php

namespace App\Factory;

use App\Entity\BankPhone;
use App\Repository\BankPhoneRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static BankPhone|Proxy findOrCreate(array $attributes)
 * @method static BankPhone|Proxy random()
 * @method static BankPhone[]|Proxy[] randomSet(int $number)
 * @method static BankPhone[]|Proxy[] randomRange(int $min, int $max)
 * @method static BankPhoneRepository|RepositoryProxy repository()
 * @method BankPhone|Proxy create($attributes = [])
 * @method BankPhone[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class BankPhoneFactory extends ModelFactory
{
    /**
     * @return self
     */
    public function setBank(array $types): CounterpartPhoneFactory
    {
        return $this->addState(
            [
                'type' => self::faker()->randomElement($types),
            ]
        );
    }

    protected function getDefaults(): array
    {
        return [
            'phone' => self::faker()->unique()->e164PhoneNumber,
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this;
    }

    protected static function getClass(): string
    {
        return BankPhone::class;
    }
}
