<?php

namespace App\Factory;

use App\Entity\CounterpartContract;
use App\Repository\CounterpartContractRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static CounterpartContract|Proxy findOrCreate(array $attributes)
 * @method static CounterpartContract|Proxy random()
 * @method static CounterpartContract[]|Proxy[] randomSet(int $number)
 * @method static CounterpartContract[]|Proxy[] randomRange(int $min, int $max)
 * @method static CounterpartContractRepository|RepositoryProxy repository()
 * @method CounterpartContract|Proxy create($attributes = [])
 * @method CounterpartContract[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class CounterpartContractFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->title,
            'number' => self::faker()->randomNumber(8),
            'contractDate' => self::faker()->dateTimeBetween('-2 months'),
            'obligationDate' => self::faker()->dateTimeBetween('-2 weeks'),
            'obligationLiquidationDate' => self::faker()->dateTimeBetween('-2 days'),
            'fineStartDate' => self::faker()->dateTimeBetween('-1 weeks'),
            'fineEndDate' => self::faker()->dateTimeBetween('-1 days'),
            'fineTimePeriodType' => TimePeriodUnitFactory::repository()->random(),
            'fineRate' => self::faker()->numberBetween(101, 99999),
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this// ->afterInstantiate(function(CounterpartContract $counterpartContract) {})
            ;
    }

    protected static function getClass(): string
    {
        return CounterpartContract::class;
    }
}
