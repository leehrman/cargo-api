<?php

namespace App\Factory;

use App\Entity\CounterpartCategory;
use App\Repository\CounterpartCategoryRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static CounterpartCategory|Proxy findOrCreate(array $attributes)
 * @method static CounterpartCategory|Proxy random()
 * @method static CounterpartCategory[]|Proxy[] randomSet(int $number)
 * @method static CounterpartCategory[]|Proxy[] randomRange(int $min, int $max)
 * @method static CounterpartCategoryRepository|RepositoryProxy repository()
 * @method CounterpartCategory|Proxy create($attributes = [])
 * @method CounterpartCategory[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class CounterpartCategoryFactory extends ModelFactory
{
    /**
     * @param array $categories
     * @return CounterpartCategoryFactory
     */
    public function setRandomParent(array $categories): CounterpartCategoryFactory
    {
        return $this->addState(
            [
                'parent' => self::faker()->randomElement($categories),
            ]
        );
    }

    protected function getDefaults(): array
    {
        return [
            'name' => 'Folder ' . self::faker()->unique()->numberBetween(0, 20),
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this;
    }

    protected static function getClass(): string
    {
        return CounterpartCategory::class;
    }
}
