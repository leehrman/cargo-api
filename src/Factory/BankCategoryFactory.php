<?php

namespace App\Factory;

use App\Entity\BankCategory;
use App\Repository\BankCategoryRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static BankCategory|Proxy findOrCreate(array $attributes)
 * @method static BankCategory|Proxy random()
 * @method static BankCategory[]|Proxy[] randomSet(int $number)
 * @method static BankCategory[]|Proxy[] randomRange(int $min, int $max)
 * @method static BankCategoryRepository|RepositoryProxy repository()
 * @method BankCategory|Proxy create($attributes = [])
 * @method BankCategory[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class BankCategoryFactory extends ModelFactory
{
    /**
     * @param array $categories
     * @return self
     */
    public function setRandomParent(array $categories): self
    {
        return $this->addState(
            [
                'parent' => self::faker()->randomElement($categories),
            ]
        );
    }

    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->city,
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this;
    }

    protected static function getClass(): string
    {
        return BankCategory::class;
    }
}
