<?php

namespace App\Factory;

use App\Entity\CounterpartContactPerson;
use App\Repository\CounterpartContactPersonRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static CounterpartContactPerson|Proxy findOrCreate(array $attributes)
 * @method static CounterpartContactPerson|Proxy random()
 * @method static CounterpartContactPerson[]|Proxy[] randomSet(int $number)
 * @method static CounterpartContactPerson[]|Proxy[] randomRange(int $min, int $max)
 * @method static CounterpartContactPersonRepository|RepositoryProxy repository()
 * @method CounterpartContactPerson|Proxy create($attributes = [])
 * @method CounterpartContactPerson[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class CounterpartContactPersonFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->firstName . ' ' . self::faker()->lastName,
            'position' => self::faker()->jobTitle,
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this;
    }

    protected static function getClass(): string
    {
        return CounterpartContactPerson::class;
    }
}
