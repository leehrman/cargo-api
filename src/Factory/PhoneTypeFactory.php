<?php

namespace App\Factory;

use App\Entity\PhoneType;
use App\Repository\PhoneTypeRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static PhoneType|Proxy findOrCreate(array $attributes)
 * @method static PhoneType|Proxy random()
 * @method static PhoneType[]|Proxy[] randomSet(int $number)
 * @method static PhoneType[]|Proxy[] randomRange(int $min, int $max)
 * @method static PhoneTypeRepository|RepositoryProxy repository()
 * @method PhoneType|Proxy create($attributes = [])
 * @method PhoneType[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class PhoneTypeFactory extends ModelFactory
{

    protected function getDefaults(): array
    {
        return [];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this;
    }

    protected static function getClass(): string
    {
        return PhoneType::class;
    }
}
