<?php

namespace App\Factory;

use App\Entity\CounterpartPhone;
use App\Repository\CounterpartPhoneRepository;
use App\Entity\PhoneType;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static CounterpartPhone|Proxy findOrCreate(array $attributes)
 * @method static CounterpartPhone|Proxy random()
 * @method static CounterpartPhone[]|Proxy[] randomSet(int $number)
 * @method static CounterpartPhone[]|Proxy[] randomRange(int $min, int $max)
 * @method static CounterpartPhoneRepository|RepositoryProxy repository()
 * @method CounterpartPhone|Proxy create($attributes = [])
 * @method CounterpartPhone[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class CounterpartPhoneFactory extends ModelFactory
{
    /**
     * @param PhoneType[] $types
     * @return self
     */
    public function setRandomType(array $types): CounterpartPhoneFactory
    {
        return $this->addState(
            [
                'type' => self::faker()->randomElement($types),
            ]
        );
    }

    protected function getDefaults(): array
    {
        $phoneTypes = PhoneTypeFactory::repository()->findAll();

        return [
            'phone' => self::faker()->unique()->e164PhoneNumber,
            'type' => self::faker()->randomElement($phoneTypes),
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this;
    }

    protected static function getClass(): string
    {
        return CounterpartPhone::class;
    }
}
