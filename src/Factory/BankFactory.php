<?php

namespace App\Factory;

use App\Entity\Bank;
use App\Repository\BankRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Bank|Proxy findOrCreate(array $attributes)
 * @method static Bank|Proxy random()
 * @method static Bank[]|Proxy[] randomSet(int $number)
 * @method static Bank[]|Proxy[] randomRange(int $min, int $max)
 * @method static BankRepository|RepositoryProxy repository()
 * @method Bank|Proxy create($attributes = [])
 * @method Bank[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class BankFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        $category = BankCategoryFactory::repository()->random();

        return [
            'name' => 'Банк' . self::faker()->company,
            'category' => $category,
            'location' => self::faker()->city . ' ' . self::faker()->city,
            'bic' => $this->generateRandomNumber(9),
            'correspondentAccount' => $this->generateRandomNumber(20),
            'address' => self::faker()->address,
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            ->afterPersist(
                static function (Proxy $bank, array $attributes) {
                    BankPhoneFactory::new()->addState(['bank' => $bank])->createMany(2);
                }
            );
    }

    protected static function getClass(): string
    {
        return Bank::class;
    }

    /**
     * @param string $length
     * @return string
     */
    private function generateRandomNumber(int $length = null): string
    {
        $length ??= 1;
        $number = '';
        for ($i = 1; $i <= $length; $i++) {
            $number .= self::faker()->numberBetween(0, 9);
        }

        return $number;
    }
}
