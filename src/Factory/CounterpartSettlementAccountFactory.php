<?php

namespace App\Factory;

use App\Entity\CounterpartSettlementAccount;
use App\Repository\CounterpartSettlementAccountRepository;
use Faker\Factory;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static CounterpartSettlementAccount|Proxy findOrCreate(array $attributes)
 * @method static CounterpartSettlementAccount|Proxy random()
 * @method static CounterpartSettlementAccount[]|Proxy[] randomSet(int $number)
 * @method static CounterpartSettlementAccount[]|Proxy[] randomRange(int $min, int $max)
 * @method static CounterpartSettlementAccountRepository|RepositoryProxy repository()
 * @method CounterpartSettlementAccount|Proxy create($attributes = [])
 * @method CounterpartSettlementAccount[]|Proxy[] createMany(int $number, $attributes = [])
 */
final class CounterpartSettlementAccountFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        $banks = BankFactory::randomSet(2);
        return [
            'name' => 'Р/С в ' . self::faker()->company . 'Банк',
            'number' => $this->generateNumber(),
            'correspondent' => 'OOO "' . self::faker()->company . '"',
            'appointment' => self::faker()->text(100),
            'showReasonCode' => self::faker()->boolean,
            'accountBank' => $banks[0],
            'correspondentBank' => $banks[1],
            'counterpart' => CounterpartFactory::random()
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this;
    }

    protected static function getClass(): string
    {
        return CounterpartSettlementAccount::class;
    }

    /**
     * @return string
     */
    private function generateNumber(): string
    {
        $number = '';
        for ($i = 1; $i <= 20; $i++) {
            $number .= self::faker()->numberBetween(0, 9);
        }

        return $number;
    }
}
